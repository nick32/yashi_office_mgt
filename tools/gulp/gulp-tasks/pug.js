var gulp = require('gulp'),
    pug  = require('gulp-pug'),
    data = require('gulp-data'),
    fs   = require('fs'),
    gulpif    = require('gulp-if'),
    readConfig = require('read-config'),
    config    = readConfig('./config.json');

/*
* BUILD ALL HTML FILES FOR ALL THEMES
*/

gulp.task('build:pug', [
    'admin_1:pug',
    'admin_1_bootstrap3:pug',
    'admin_2:pug',
    'admin_2_bootstrap3:pug',
    'admin_3:pug',
]);

/*
* BUILD ALL HTML FILES FOR ADMIN_1 Bootstrap 4 variant THEME
*/

gulp.task('admin_1:pug', function() {
    return gulp.src(config.build.admin_1.pug.src)
        .pipe(data(function(file){
            return JSON.parse(fs.readFileSync(config.path.src+'/pug/menu.json'))
        }))
        .pipe(pug({ 
            pretty: true,
        }).on('error', function(err) {
            console.log(err);
        }))
        .pipe(gulp.dest(config.build.admin_1.pug.output));
});

/*
* BUILD ALL HTML FILES FOR ADMIN_1 Bootstrap 3 variant THEME
*/

gulp.task('admin_1_bootstrap3:pug', function() {
    return gulp.src(config.build.admin_1_bootstrap3.pug.src)
        .pipe(data(function(file){
            return JSON.parse(fs.readFileSync(config.path.src+'/pug/menu.json'))
        }))
        .pipe(pug({ 
            pretty: true,
        }).on('error', function(err) {
            console.log(err);
        }))
        .pipe(gulp.dest(config.build.admin_1_bootstrap3.pug.output));
});

/*
* BUILD ALL HTML FILES FOR ADMIN_2 Bootstrap 4 variant THEME
*/

gulp.task('admin_2:pug', function() {
    return gulp.src(config.build.admin_2.pug.src)
        .pipe(data(function(file){
            return JSON.parse(fs.readFileSync(config.path.src+'/pug/menu.json'))
        }))
        .pipe(pug({ 
            pretty: true,
        }).on('error', function(err) {
            console.log(err);
        }))
        .pipe(gulp.dest(config.build.admin_2.pug.output));
});

/*
* BUILD ALL HTML FILES FOR ADMIN_2 Bootstrap 3 variant THEME
*/

gulp.task('admin_2_bootstrap3:pug', function() {
    return gulp.src(config.build.admin_2_bootstrap3.pug.src)
        .pipe(data(function(file){
            return JSON.parse(fs.readFileSync(config.path.src+'/pug/menu.json'))
        }))
        .pipe(pug({ 
            pretty: true,
        }).on('error', function(err) {
            console.log(err);
        }))
        .pipe(gulp.dest(config.build.admin_2_bootstrap3.pug.output));
});

/*
* BUILD ALL HTML FILES FOR ADMIN_3 Bootstrap 4 variant THEME
*/

gulp.task('admin_3:pug', function() {
    return gulp.src(config.build.admin_3.pug.src)
        .pipe(data(function(file){
            return JSON.parse(fs.readFileSync(config.path.src+'/pug/menu.json'))
        }))
        .pipe(pug({ 
            pretty: true,
        }).on('error', function(err) {
            console.log(err);
        }))
        .pipe(gulp.dest(config.build.admin_3.pug.output));
});
