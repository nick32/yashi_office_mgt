var gulp = require('gulp'),
    sass         = require('gulp-sass'),
    cleanCSS     = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer'),
    concat       = require('gulp-concat'),
    rename       = require('gulp-rename'),
    bourbon      = require('node-bourbon'),
    gulpif       = require('gulp-if'),
    readConfig = require('read-config'),
    config    = readConfig('./config.json');


/*
* BUILD ALL CSS FILES FOR ALL THEMES
*/

gulp.task('build:sass', [
    'admin_1:sass',
    'admin_1_bootstrap3:sass',
    'admin_1_angular:sass',
    'admin_2:sass',
    'admin_2_bootstrap3:sass',
    'admin_2_angular:sass',
    'admin_3:sass',
    'admin_3_angular:sass',
]);

/*
* BUILD ALL CSS FILES FOR ADMIN_1 Bootstrap 4 variant THEME
*/

gulp.task('admin_1:sass', function(){
    /* 
    * MAIN CSS FILE FOR ADMIN_1 Bootstrap 4 variant: included
    *   bootstrap_4/main.scss        -- ui components
    *   layouts/admin_1/layout.scss  -- admin_1 theme layout
    *   vendors/vendors.scss         -- reset vendor plugins styles
    */
    gulp.src(config.build.admin_1.sass.src.bundle)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(concat('main.css'))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_1.sass.output.bundle));

    //== THEME SKIN FILES
    gulp.src(config.build.admin_1.sass.src.themes)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_1.sass.output.themes));

    //== PAGES STYLES
    gulp.src(config.build.admin_1.sass.src.pages)
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_1.sass.output.pages));
});

/*
* BUILD ALL CSS FILES FOR ADMIN_1 Bootstrap 3 variant THEME
*/

gulp.task('admin_1_bootstrap3:sass', function(){
    /* 
    * MAIN CSS FILE FOR ADMIN_1 Bootstrap 3 variant: included
    *   bootstrap_3/main.scss        -- ui components
    *   layouts/admin_1/layout.scss  -- admin_1 Bootstrap 3 variant theme layout
    *   vendors/vendors.scss         -- reset vendor plugins styles
    */
    gulp.src(config.build.admin_1_bootstrap3.sass.src.bundle)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(concat('main.css'))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_1_bootstrap3.sass.output.bundle));

    // THEME SKIN FILES
    gulp.src(config.build.admin_1_bootstrap3.sass.src.themes)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_1_bootstrap3.sass.output.themes));

    //== PAGES STYLES
    gulp.src(config.build.admin_1_bootstrap3.sass.src.pages)
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_1_bootstrap3.sass.output.pages));
});

/*
* BUILD ALL CSS FILES FOR ADMIN_1 Bootstrap 4 Adnular 4 variant THEME
*/

gulp.task('admin_1_angular:sass', function(){
    /* 
    * MAIN CSS FILE FOR ADMIN_1 Bootstrap 4 Angular 4 variant: included
    *   bootstrap_4/main.scss        -- ui components
    *   layouts/admin_1/layout.scss  -- admin_1 theme layout
    *   vendors/vendors.scss         -- reset vendor plugins styles
    */
    gulp.src(config.build.admin_1_angular.sass.src.bundle)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(concat('main.css'))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_1_angular.sass.output.bundle));
    
    // THEME SKIN FILES
    gulp.src(config.build.admin_1_angular.sass.src.themes)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_1_angular.sass.output.themes));

    //== PAGES STYLES
    gulp.src(config.build.admin_1_angular.sass.src.pages)
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_1_angular.sass.output.pages));
});

/*
* BUILD ALL CSS FILES FOR ADMIN_2 Bootstrap 4 variant THEME
*/

gulp.task('admin_2:sass', function(){
    /* 
    * MAIN CSS FILE FOR ADMIN 2 Bootstrap 4 variant: included
    *   bootstrap_4/main.scss        -- ui components
    *   layouts/admin_2/layout.scss  -- admin_2 theme layout
    *   vendors/vendors.scss         -- reset vendor plugins styles
    */
    gulp.src(config.build.admin_2.sass.src.bundle)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(concat('main.css'))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_2.sass.output.bundle));

    // THEME SKIN FILES
    gulp.src(config.build.admin_2.sass.src.themes)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_2.sass.output.themes));

    //== PAGES STYLES
    gulp.src(config.build.admin_2.sass.src.pages)
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_2.sass.output.pages));
});

/*
* BUILD ALL CSS FILES FOR ADMIN_2 Bootstrap 3 variant THEME
*/

gulp.task('admin_2_bootstrap3:sass', function(){
    /* 
    * MAIN CSS FILE FOR ADMIN 2 Bootstrap 3 variant: included
    *   bootstrap_3/main.scss        -- ui components
    *   layouts/admin_1/layout.scss  -- admin_2 Bootstrap 3 variant theme layout
    *   vendors/vendors.scss         -- reset vendor plugins styles
    */
    gulp.src(config.build.admin_2_bootstrap3.sass.src.bundle)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(concat('main.css'))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_2_bootstrap3.sass.output.bundle));
    
    // THEME SKIN FILES
    gulp.src(config.build.admin_2_bootstrap3.sass.src.themes)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_2.sass.output.themes));

    //== PAGES STYLES
    gulp.src(config.build.admin_2_bootstrap3.sass.src.pages)
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_2_bootstrap3.sass.output.pages));
});

/*
* BUILD ALL CSS FILES FOR ADMIN_2 Bootstrap 4 Angular 4 variant THEME
*/

gulp.task('admin_2_angular:sass', function(){
    /* 
    * MAIN CSS FILE FOR ADMIN_2 Bootstrap 4 Angular 4 variant: included
    *   bootstrap_4/main.scss        -- ui components
    *   layouts/admin_2/layout.scss  -- admin_2 theme layout
    *   vendors/vendors.scss         -- reset vendor plugins styles
    */
    gulp.src(config.build.admin_2_angular.sass.src.bundle)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(concat('main.css'))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_2_angular.sass.output.bundle));

    // THEME SKIN FILES

    gulp.src(config.build.admin_2_angular.sass.src.themes)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_2_angular.sass.output.themes));

    //== PAGES STYLES
    gulp.src(config.build.admin_2_angular.sass.src.pages)
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_2_angular.sass.output.pages));
});

/*
* BUILD ALL CSS FILES FOR ADMIN_3 Bootstrap 4 variant THEME
*/

gulp.task('admin_3:sass', function(){
    /* 
    * MAIN CSS FILE FOR ADMIN_3 Bootstrap 4 variant: included
    *   bootstrap_4/main.scss        -- ui components
    *   layouts/admin_3/layout.scss  -- admin_3 theme layout
    *   vendors/vendors.scss         -- reset vendor plugins styles
    */
    gulp.src(config.build.admin_3.sass.src.bundle)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(concat('main.css'))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_3.sass.output.bundle));
    
    // THEME SKIN FILES

    gulp.src(config.build.admin_3.sass.src.themes)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_3.sass.output.themes));

    //== PAGES STYLES
    gulp.src(config.build.admin_3.sass.src.pages)
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_3.sass.output.pages));
});

/*
* BUILD ALL CSS FILES FOR ADMIN_3 Bootstrap 4 Angular 4 variant THEME
*/

gulp.task('admin_3_angular:sass', function(){
    /* 
    * MAIN CSS FILE FOR ADMIN_3 Bootstrap 4 Angular 4 variant: included
    *   bootstrap_4/main.scss        -- ui components
    *   layouts/admin_3/layout.scss  -- admin_3 theme layout
    *   vendors/vendors.scss         -- reset vendor plugins styles
    */
    gulp.src(config.build.admin_3_angular.sass.src.bundle)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(concat('main.css'))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_3_angular.sass.output.bundle));
    
    // THEME SKIN FILES

    gulp.src(config.build.admin_3_angular.sass.src.themes)
    .pipe(sass({
        includePaths: bourbon.includePaths
    }))
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_3_angular.sass.output.themes));

    //== PAGES STYLES
    gulp.src(config.build.admin_3_angular.sass.src.pages)
    .pipe(gulpif(config.compile.cssMinify, cleanCSS({compatibility: 'ie8'})))
    .pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
    }))
    .pipe(gulp.dest(config.build.admin_3_angular.sass.output.pages));
});
