var gulp = require('gulp'),
    uglify    = require('gulp-uglify'),
    concat    = require('gulp-concat'),
    rename    = require('gulp-rename'),
    jshint    = require('gulp-jshint'),
    gulpif    = require('gulp-if'),
    readConfig = require('read-config'),
    config    = readConfig('./config.json'),
    args = require('yargs').argv;

/*
* BUILD ALL JS FILES FOR ALL THEMES
*/

gulp.task('build:js',[
    'admin_1:js',
    'admin_1_bootstrap3:js',
    'admin_2:js',
    'admin_2_bootstrap3:js',
    'admin_3:js',
]);

/*
* BUILD JS FILES FOR ADMIN_1 Bootstrap 4 variant THEME
*/

gulp.task('admin_1:js', function() {
   gulp.src(config.build.admin_1.js.src)
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'))
    .pipe(gulpif(config.compile.jsUglify, uglify()))
    .pipe(concat('app.js'))
    .pipe(gulp.dest(config.build.admin_1.js.output));

   //== Page level scripts
   gulp.src(config.path.src+'/js/theme/admin_1/scripts/**/*.js')
    .pipe(gulp.dest(config.build.admin_1.js.output+'/scripts'));
});

/*
* BUILD JS FILES FOR ADMIN_1 Bootstrap 3 variant THEME
*/

gulp.task('admin_1_bootstrap3:js', function() {
   gulp.src(config.build.admin_1_bootstrap3.js.src)
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'))
    .pipe(gulpif(config.compile.jsUglify, uglify()))
    .pipe(concat('app.js'))
    .pipe(gulp.dest(config.build.admin_1_bootstrap3.js.output));

   //== Page level scripts
   gulp.src(config.path.src+'/js/theme/admin_1_bootstrap3/scripts/**/*.js')
    .pipe(gulp.dest(config.build.admin_1_bootstrap3.js.output+'/scripts'));
});

/*
* BUILD JS FILES FOR ADMIN_2 Bootstrap 4 variant THEME
*/

gulp.task('admin_2:js', function() {
   gulp.src(config.build.admin_2.js.src)
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'))
    .pipe(gulpif(config.compile.jsUglify, uglify()))
    .pipe(concat('app.js'))
    .pipe(gulp.dest(config.build.admin_2.js.output));

   //== Page level scripts
   gulp.src(config.path.src+'/js/theme/admin_2/scripts/**/*.js')
    .pipe(gulp.dest(config.build.admin_2.js.output+'/scripts'));
});

/*
* BUILD JS FILES FOR ADMIN_2 Bootstrap 3 variant THEME 
*/

gulp.task('admin_2_bootstrap3:js', function() {
   gulp.src(config.build.admin_2_bootstrap3.js.src)
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'))
    .pipe(gulpif(config.compile.jsUglify, uglify()))
    .pipe(concat('app.js'))
    .pipe(gulp.dest(config.build.admin_2_bootstrap3.js.output));

   //== Page level scripts
   gulp.src(config.path.src+'/js/theme/admin_2_bootstrap3/scripts/**/*.js')
    .pipe(gulp.dest(config.build.admin_2_bootstrap3.js.output+'/scripts'));
});

/*
* BUILD JS FILES FOR ADMIN_3 Bootstrap 4 variant THEME
*/

gulp.task('admin_3:js', function() {
   gulp.src(config.build.admin_3.js.src)
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'))
    .pipe(gulpif(config.compile.jsUglify, uglify()))
    .pipe(concat('app.js'))
    .pipe(gulp.dest(config.build.admin_3.js.output));

   //== Page level scripts
   gulp.src(config.path.src+'/js/theme/admin_3/scripts/**/*.js')
    .pipe(gulp.dest(config.build.admin_3.js.output+'/scripts'));
});

