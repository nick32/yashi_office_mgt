import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Http } from '@angular/http';
declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../../../assets/css/pages/auth-light.css']
})
export class LoginComponent implements OnInit, OnDestroy, AfterViewInit {

  constructor(private http: Http) { }

  ngAfterViewInit() {
    $('#login-form').validate({
        errorClass:"help-block",
        rules: {
            email: {required:true,email:true},
            password: {required:true}
        },
        highlight:function(e){$(e).closest(".form-group").addClass("has-error")},
        unhighlight:function(e){$(e).closest(".form-group").removeClass("has-error")},
    });
  }
  ngOnInit(): void {
        this.http.get('http://localhost/yashi_office_mgt/users/login').subscribe();
      $('body').css({'background-color':'#2CC4CB', 'background-image':'linear-gradient(135deg,#2CC4CB,#18C5A9)'});
  }
  ngOnDestroy() { 
      $('body').css({'background-color':'', 'background-image':''});
  }

}
