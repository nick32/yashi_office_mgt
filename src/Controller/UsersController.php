<?php

namespace App\Controller;

use Cake\Network\Exception\NotFoundException;
use Cake\Routing\Router;

/**
 * Description of UsersController
 *
 * @author Rauxmedia
 */
class UsersController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        
        //$this->Auth->allow(["sso", 'login']);
    }

    public $paginate = [
        'limit' => 10
    ];

    public function login($resendotp = null) {
        
        
         $response = array(
             "status"=>true,
            "success" => true,
             "data"=>["test"=>"tesdgtsfv asd fc"]
        );
       
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if (($user['status'] == 1)) {

                    $Activestatus = $this->moduleStatus("site-security", true);

                    $this->Auth->setUser($user);
                }
                if ($Activestatus) {
                    //     check ip address for login 
                    if ($this->checkAuthIpAddress($user)) {
                        $this->redirect($this->Auth->redirectUrl());
                    } else {
                        $this->Flash->error(__('You are not in same network!'));
                    }
                } elseif (!$otp) {
                    $this->redirect($this->Auth->redirectUrl());
                }
            } else {
                $this->Flash->error(__('Invalid username or password, try again'));
            }
        }
         $this->json($response);
        
    }
    
     public function registration() {
          
        $this->viewBuilder()->layout(false);
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if (($user['status'] == 1)) {

                    $Activestatus = $this->moduleStatus("site-security", true);

                    $this->Auth->setUser($user);
                }
                if ($Activestatus) {
                    //     check ip address for login 
                    if ($this->checkAuthIpAddress($user)) {
                        $this->redirect($this->Auth->redirectUrl());
                    } else {
                        $this->Flash->error(__('You are not in same network!'));
                    }
                } elseif (!$otp) {
                    $this->redirect($this->Auth->redirectUrl());
                }
            } else {
                $this->Flash->error(__('Invalid username or password, try again'));
            }
        }
        return $this->redirect('/register');
    }
    

}
